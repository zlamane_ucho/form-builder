export const prepareDataBase = () => {
  let idb_req = window.indexedDB.open("cells-db", 1);
  idb_req.addEventListener("upgradeneeded", e => {
    let db = e.target.result;
    db.createObjectStore("cells-data");
    alert("Store is ready!");
  });

  return idb_req;
};

export const addItem = (req, item) => {
  let db = req.result;

  let tx = db.transaction("cells-data", "readwrite");

  let store = tx.objectStore("cells-data");

  // console.log(item);
  let storeReq = store.get(item.key);

  storeReq.addEventListener("success", e => {
    if (!storeReq.result) {
      store.add(item, item.key);
    } else {
      store.put(item, item.key);
    }
  });
};

export const deleteItem = (req, key) => {
  let db = req.result;

  let tx = db.transaction("cells-data", "readwrite");

  let store = tx.objectStore("cells-data");

  store.delete(key);
};

export const getItems = req => {
  let idb_data = {};
  console.log(req);
  req.addEventListener("success", e => {
    let db = req.result;
    console.log(console.log(db));

    let tx = e.target.result.transaction("cells-data", "readwrite");
    console.log(tx);

    let store = tx.objectStore("cells-data");
    console.log(store);

    let storeReq = store.getAll();
    storeReq.addEventListener("success", e => {
      idb_data = e.target.result;
      // console.log("DATA", idb_data);
      return prepareData(idb_data);
    });
  });
};

export const prepareData = data => {
  let preparedData = {};

  data.forEach(element => {
      preparedData[element.key] = element;
  });

  return prepareData
}
