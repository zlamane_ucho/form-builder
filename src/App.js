import React, { Component } from "react";
import "./styles/bootstrap.min.css";
import "./App.css";
import FormCell from "./components/FormCell";

import { connect } from "react-redux";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formCells: []
    };

    this.formCellsKeys = [];
  }

  componentWillMount() {
    //GET ASYNC DATA FROM IDB
    let idb_data = {};
    let req = this.props.request;
    // console.log(req);
    req.addEventListener("success", e => {
      let db = req.result;
      // console.log(db);

      let tx = e.target.result.transaction("cells-data", "readwrite");
      // console.log(tx);

      let store = tx.objectStore("cells-data");
      // console.log(store);

      let storeReq = store.getAll();
      storeReq.addEventListener("success", e => {
        idb_data = e.target.result;
        //SEND IT TO REDUX
        this.props.onGetData(idb_data);

        this.prepareUI();
      });
    });
  }

  prepareUI = () => {
    let cells = { ...this.props.cells };
    // console.log("CELLS", cells);

    let formCellsKeys = Object.keys(cells).filter(key => {
      return !cells[key].hasOwnProperty("parents");
    });

    // console.log("KEYS", formCellsKeys);

    let rendThis = this.prepareRecursion(cells, [...formCellsKeys]);
    // console.log("REND THIS", rendThis);

    this.setState({
      formCells: rendThis
    });
  };

  prepareRecursion = (cells, keys) => {
    // console.log(cells, keys);

    let UI_to_rend = [];
    let cellComp = null;

    keys.forEach(key => {
      if (cells[key].subFormCells) {
        let subCellsArr = this.prepareRecursion(cells, cells[key].subFormCells);
        cells[key].subFormCells = subCellsArr;
        cellComp = (
          <FormCell
            key={key}
            idb_data={cells[key]}
            deleteHandler={() => this.deleteButtonHandler(key)}
          />
        );
        UI_to_rend.push(cellComp);
      } else {
        cellComp = (
          <FormCell
            key={key}
            idb_data={cells[key]}
            deleteHandler={() => this.deleteButtonHandler(key)}
          />
        );
        UI_to_rend.push(cellComp);
      }
    });

    return UI_to_rend;
  };

  addButtonHandler = e => {
    e.preventDefault();
    //prepare key
    let key = `_${Math.random()
      .toString(36)
      .substr(2, 9)}`;
    //prepare element
    let formCell = (
      <FormCell
        key={key}
        keyish={key}
        deleteHandler={() => this.deleteButtonHandler(key)}
      />
    );

    this.setState((state, props) => {
      //spread actuall array and add element
      let tempFormCells = [...state.formCells, formCell];
      //return new state
      return {
        ...state,
        formCells: tempFormCells
      };
    });

    //No outline after click
    e.target.blur();
  };

  deleteButtonHandler = key => {
    //delete clicked element
    let tempFormCells = [...this.state.formCells].filter(item => {
      return item.key !== key;
    });

    //set new state
    this.setState({
      formCells: tempFormCells
    });

    this.props.onCellDelete(key);
  };

  render() {
    return (
      <div className="container">
        <h1>Build your form</h1>

        {this.state.formCells}

        <button
          className="btn btn-success"
          onClick={e => this.addButtonHandler(e)}
        >
          Add form cell!
        </button>

      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cells: state.cells,
    request: state.idb_req
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCellDelete: key => dispatch({ type: "CELL_DELETE", key: key }),
    onGetData: data => dispatch({ type: "GET_DATA", data: data })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
