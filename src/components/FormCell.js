import React, { Component } from "react";
import SubFormCell from './SubFormCell';
import {connect} from 'react-redux';


class FormCell extends Component {
  constructor(props) {
    super(props)

    this.state = {
      subFormCells: [],
      type: "text",
      question: "",
      key: props.keyish
    }
  }

  componentWillMount(){
    if(this.props.idb_data) {
      this.setState(
        {
          ...this.props.idb_data
        }
      )
    }
  }

  componentDidMount(){
    if(this.props.onCellUpdate) this.props.onCellUpdate({...this.state});
  }
  
  componentDidUpdate(prevProps, prevState) {
   if(prevState !== this.state && this.props.onCellUpdate) this.props.onCellUpdate({...this.state});
  }

  addButtonHandler = e => {
    e.preventDefault();

    this.setState((state, props) => {
      //prepare key
      let key = `_${Math.random()
        .toString(36)
        .substr(2, 9)}`;
      //prepare element
      let subFormCell = (
        <SubFormCell
          key={key}
          keyish={key}
          type={this.state.type}
          parents={[this.state.key]}
          deleteHandler={() => this.deleteButtonHandler(key)}
        />
      );
      //spread actuall array and add element
      let tempSubFormCells = [...state.subFormCells, subFormCell];
      //return new state
      return {
        ...state,
        subFormCells: tempSubFormCells
      };
    });

    //No outline after click
    e.target.blur();
  };

  deleteButtonHandler = key => {

    //delete clicked element
    let tempSubFormCells = [...this.state.subFormCells].filter(item => {
      return item.key !== key;
    });

    //set new state
    this.setState({
      subFormCells: tempSubFormCells,
    });

    this.props.onCellDelete(key);
  };

  selectHandler = e => {
    //change props of sub cells
    let temp = [...this.state.subFormCells];
    temp = temp.map(el => {
      return React.cloneElement(
        el,
        {type: e.target.value}
      )
    });

    this.setState({
      type: e.target.value,
      subFormCells: temp
    });
  }

  questionHandler = e => {
    this.setState({
      question: e.target.value
    })
  }

  render() {
    return (
      <div className="container form-ver-margin form-border">
        <form>
          <div className="row">
            <div className="col-lg-10">
              <div className="form-group row">
                <label className="col-lg-1 col-form-label">Question</label>
                <div className="col-lg-11">
                  <input type="text" className="form-control" value={this.state.question} onChange={this.questionHandler}/>
                </div>
              </div>
            </div>

            <div className="col-lg-2">
            <div className="form-group row">
              <label className="col-lg-3 col-form-label">Type</label>
              <div className="col-lg-9">
                <select className="form-control" id="question-type" onChange = {this.selectHandler}>
                  <option value = "text">Text</option>
                  <option value = "number">Number</option>
                  <option value = "boolean">Yes/No</option>
                </select>
              </div>
            </div>
            </div>

            
          </div>

          <div className="row justify-content-center">
            <button
              type="button"
              className="btn btn-success btn-sm col-lg-2 col-md-10 col-sm-10 btn-hor-margin"
              onClick = {this.addButtonHandler}
            >
              Add sub-form cell
            </button>
            <button
              type="button"
              className="btn btn-danger btn-sm col-lg-2 col-md-10 col-sm-10 btn-hor-margin"
              onClick = {this.props.deleteHandler}
            >
              Delete!
            </button>
          </div>
        </form>

        {this.state.subFormCells}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
      cells: state,
      request: state.idb_req
  }
};

const mapDispatchToProps = dispatch => {
  return {
      onCellUpdate: (cell) => dispatch({type: "CELL_UPDATE", cell: cell}),
      onCellDelete: (key) => dispatch({type: "CELL_DELETE", key: key}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormCell);
