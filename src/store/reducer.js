import {
  prepareDataBase,
  addItem,
  deleteItem,
} from "../idb_service/idb_service";

const initState = {
  idb_req: prepareDataBase(),
  cells: {},
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case "CELL_UPDATE":
      //map React component to its key
      let subCells = action.cell.subFormCells.map(el => {
        return el.key;
      });

      let objToSend = { ...action.cell };
      objToSend.subFormCells = subCells;

      //add correct JS obj to IndexedDB
      addItem(state.idb_req, objToSend);

      return {
        ...state,
        cells: {
          ...state.cells,
          [action.cell.key]: action.cell
        }
      };
    case "CELL_DELETE":
      // improvement
      let updatedCells = deleteRecursion({ ...state.cells }, action.key, state.idb_req);
      
      ///>> First try... searching by Obj key. 
      ///>> Property "parents" specially created for this method
    //   let tempCells = { ...state.cells };
    //   let objToDel = Object.keys(tempCells).filter(el => {
    //     if (tempCells[el].hasOwnProperty("parents")) {
    //       if (tempCells[el].parents.includes(action.key)) return true;
    //       else return false;
    //     } else return false;
    //   });

    //   objToDel = [...objToDel, action.key];
    //   objToDel.forEach(el => {
    //     delete tempCells[el];
    //     deleteItem(state.idb_req, el);
    //   });
      return {
        ...state,
        cells: updatedCells
      };
      case "GET_DATA":
      return {
        ...state,
        cells: prepareCells(action.data),
      }

    default:
      return { ...state };
  }
};

export default reducer;

const prepareCells = (cellsArr) => {
  let preparedCells = {};

  cellsArr.forEach(el => {
    preparedCells[el.key] = el;
  })

  return preparedCells;
}

const deleteRecursion = (cells, key, db_req) => {
  if (cells[key].subFormCells.length) {
    cells[key].subFormCells.forEach(el => {
      cells = deleteRecursion(cells, el.key, db_req);
    });
    delete cells[key];
    deleteItem(db_req, key);
  } else {
    delete cells[key];
    deleteItem(db_req, key);
  }
  return cells;
};
